FROM ubuntu:16.04
#docker build -t example/example-example .
#docker run -it --rm -p 4200:4200 example/example-example

USER root

#Install curl
RUN apt-get update \
    && apt-get -y install curl

#Install supervisor
RUN apt-get update \
      && apt-get install -y sudo supervisor \
      && rm -rf /var/lib/apt/lists/*

RUN mkdir /home/app/
WORKDIR /home/app/

ARG ANGULAR_CLI_VERSION=1.0.0-beta.31

#Install node and angular cli
RUN curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
RUN apt-get update && apt-get install -y nodejs
RUN npm install -g "@angular/cli@$ANGULAR_CLI_VERSION"

COPY package.json /home/app/package.json
COPY angular-cli.json /home/app/angular-cli.json
COPY karma.conf.js /home/app/karma.conf.js
COPY src /home/app/src
COPY node_modules/ /home/app/node_modules/
RUN npm install

EXPOSE 4200
CMD ["ng", "serve", "--host", "0.0.0.0"]
