import { DynamicBoardPage } from './app.po';

describe('dynamic-board App', function() {
  let page: DynamicBoardPage;

  beforeEach(() => {
    page = new DynamicBoardPage();
  });

  it('should display one window with three cards', () => {
    page.navigateTo();
    expect(page.getAppWindows().count()).toBe(1);
    expect(page.getAppCards().count()).toBe(3);
  });

  it('should create a new horizontal window', () => {
    page.navigateTo();
    page.expectElementIsHidden('/html/body/app-root/div/app-window/div/div/div[2]/b');
    page.createHorizontalWindow();
    expect(page.getAppWindows().count()).toBe(2);
    expect(page.getAppCards().count()).toBe(6);
  });

 it('should create a new vertical window', () => {
    page.navigateTo();
    page.expectElementIsHidden('/html/body/app-root/div/app-window[1]/div/div/div[3]/b');
    page.createVerticalWindow();
    expect(page.getAppWindows().count()).toBe(2);
    expect(page.getAppCards().count()).toBe(6);
  });
});
