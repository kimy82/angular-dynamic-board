import { browser, element, by, ElementArrayFinder, ElementFinder, WebElementPromise } from 'protractor';

export class DynamicBoardPage {
  navigateTo() {
    return browser.get('/');
  };

  getAppWindows(): ElementArrayFinder {
    return element.all(by.css('app-window'));
  };

  getAppCards(): ElementArrayFinder {
    return element.all(by.css('app-card'));
  };

  expectElementIsHidden(xpath: string) {
    element(by.xpath(xpath)).isDisplayed().then((val: boolean) => {
      expect(val).toBeFalsy();
    });
  };

  createHorizontalWindow() {
    browser.executeScript('(document.getElementsByClassName("add-horizontal")[0]).getElementsByTagName("b")[0].click();');
  };

  createVerticalWindow() {
    browser.executeScript('(document.getElementsByClassName("add-vertical")[0]).getElementsByTagName("b")[0].click();');
  }
}
