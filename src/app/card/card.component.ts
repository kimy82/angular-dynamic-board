import { Component, OnInit, ViewContainerRef, Input, ElementRef, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import '../../../node_modules/jquery-ui/ui/widgets/draggable.js';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.less']
})
export class CardComponent implements OnInit {

  @Input()
  public identifier: string = null;

  @ViewChild('card')
  private _containerDiv: ElementRef;

  constructor(public _viewContainerRef: ViewContainerRef) {
  }

  ngOnInit() {
    $(this._containerDiv.nativeElement).draggable({
      drag: (event, ui) => { ui.helper.addClass('dragging'); }
    });
    if (!this.identifier) {
      this.identifier = (Math.random() * 10).toFixed(0);
    }
  }

  public setIdentifier(identifier: string){
    this.identifier = identifier;
    $(this._containerDiv.nativeElement).find('h1').html(this.identifier);
  }

}
