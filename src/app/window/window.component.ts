import {
  Component, OnInit, ElementRef,
  Input, ComponentRef, ViewChild, ComponentFactoryResolver, ChangeDetectorRef, ViewContainerRef
} from '@angular/core';
import { CardComponent } from '../card/card.component';
import { AppComponent } from '../app.component';
import * as $ from 'jquery';
import '../../../node_modules/jquery-ui/ui/widgets/droppable.js';

type windowType = 'horizontal' | 'vertical';

interface Position {
  top: number;
  left: number;
}

interface Size {
  width: number;
  height: number;
}

@Component({
  selector: 'app-window',
  templateUrl: './window.component.html',
  styleUrls: ['./window.component.less'],
  entryComponents: [
    CardComponent,
    WindowComponent,
    AppComponent
  ]
})
export class WindowComponent implements OnInit {

  private static baseWidth = 40;
  private horizontalWindow: WindowComponent = null;
  private verticalWindow: WindowComponent = null;

  @Input()
  public parent: AppComponent;

  @Input()
  public num: number;

  @Input()
  public numCardsNextWindow = 1;

  @ViewChild(CardComponent)
  private cardComponent: CardComponent;

  @ViewChild('preWindow')
  private _containerDiv: ElementRef;

  @ViewChild('dropzone')
  private _dropZone: ElementRef;

  ngOnInit() {
    $(this._containerDiv.nativeElement).css('width', (WindowComponent.baseWidth * this.num) + 'px');
    $(this._containerDiv.nativeElement).find('.flex-container').css('width', (WindowComponent.baseWidth * this.num) + 'px');
    $(this._dropZone.nativeElement).droppable({
      drop: (event, ui) => { this.newCard(ui.draggable.find('h1').html()); ui.draggable.remove(); }
    });
  };

  constructor(private _cr: ComponentFactoryResolver, private _viewContainerRef: ViewContainerRef) { };

  public newCard(identifier?: string): void {
    const c: ComponentRef<CardComponent> = this.cardComponent._viewContainerRef.createComponent(
      this._cr.resolveComponentFactory(CardComponent)
    );
    if (identifier) {
      c.instance.setIdentifier(identifier);
    }
    this.propagate();
  };

  public setNumCards(num: number) {
    this.num = num;
    $(this._containerDiv.nativeElement).css('width', (WindowComponent.baseWidth * this.num) + 'px');
    $(this._containerDiv.nativeElement).find('.flex-container').css('width', (WindowComponent.baseWidth * this.num) + 'px');
  };

  public setHorizontal(size: Size, positon: Position) {
    $(this._containerDiv.nativeElement).css('top', positon.top + 'px');
    $(this._containerDiv.nativeElement).css('left', (positon.left + size.width) + 'px');
    this.propagate();
  };

  public setVertical(size: Size, positon: Position) {
    $(this._containerDiv.nativeElement).css('top', (positon.top + size.height) + 'px');
    $(this._containerDiv.nativeElement).css('left', (positon.left) + 'px');
    this.propagate();
  };

  public newWindow(type: windowType): void {
    if (type === 'horizontal') {
      const c: ComponentRef<WindowComponent> = this._viewContainerRef.createComponent(this._cr.resolveComponentFactory(WindowComponent));
      c.instance.setNumCards(this.numCardsNextWindow);
      if (this.horizontalWindow != null) {
        c.instance.horizontalWindow = this.horizontalWindow;
      }
      c.instance.setHorizontal(this.getSize(this._containerDiv), $(this._containerDiv.nativeElement).position());
      this.horizontalWindow = c.instance;
    } else if (type === 'vertical') {
      const c: ComponentRef<WindowComponent> = this._viewContainerRef.createComponent(this._cr.resolveComponentFactory(WindowComponent));
      c.instance.setNumCards(this.numCardsNextWindow);
      if (this.verticalWindow != null) {
        c.instance.verticalWindow = this.verticalWindow;
      }
      c.instance.setVertical(this.getSize(this._containerDiv), $(this._containerDiv.nativeElement).position());
      this.verticalWindow = c.instance;
    }
  };

  private getSize(element: ElementRef): Size {
    return { width: $(element.nativeElement).width(), height: $(element.nativeElement).height() };
  };

  private propagate() {
    if (this.horizontalWindow) {
      this.horizontalWindow.setHorizontal(this.getSize(this._containerDiv), $(this._containerDiv.nativeElement).position());
    }
    if (this.verticalWindow) {
      this.verticalWindow.setVertical(this.getSize(this._containerDiv), $(this._containerDiv.nativeElement).position());
    }
  };
}
