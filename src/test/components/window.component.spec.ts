/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { WindowComponent } from '../../app/window/window.component';

import constants from '../utils/constants';
import ComponentWrapper from '../utils/component.wrapper';

describe('WindowComponent', () => {
  let component: WindowComponent;
  let fixture: ComponentFixture<WindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule(constants.ALL_PRE_CONFIGURATION).compileComponents();
  }));

  beforeEach(() => {
     component = new ComponentWrapper(WindowComponent, true).getComponentInstance();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
