/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CardComponent } from '../../app/card/card.component';
import { WindowComponent } from '../../app/window/window.component';

import constants from '../utils/constants';
import ComponentWrapper from '../utils/component.wrapper';

describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;

  beforeEach(async(() => {
     TestBed.configureTestingModule(constants.ALL_PRE_CONFIGURATION).compileComponents();
  }));

  beforeEach(() => {
    component = new ComponentWrapper(CardComponent, true).getComponentInstance();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
