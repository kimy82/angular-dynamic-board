/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from '../../app/app.component';

import constants from '../utils/constants';
import ComponentWrapper from '../utils/component.wrapper';

describe('AppComponent', () => {
  beforeEach(() => {
         TestBed.configureTestingModule(constants.ALL_PRE_CONFIGURATION).compileComponents();
  });

  it('should create the app', async(() => {
    const app: AppComponent = new ComponentWrapper(AppComponent, true).getComponentInstance();
    expect(app).toBeTruthy();
  }));

  it('should render title in a h1 tag', async(() => {
    const compiled = new ComponentWrapper(AppComponent, true).getFixture().debugElement.nativeElement;
    expect(compiled.querySelector('.main')).toBeTruthy();
  }));
});
