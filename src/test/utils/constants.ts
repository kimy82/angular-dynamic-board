import { MockBackend } from '@angular/http/testing';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { BaseRequestOptions, Http, HttpModule } from '@angular/http';

import { AppComponent } from '../../app/app.component';
import { CardComponent } from '../../app/card/card.component';
import { WindowComponent } from '../../app/window/window.component';

export const DECLARATIONS = [
    AppComponent,
    CardComponent,
    WindowComponent
];
export const IMPORTS = [
    BrowserModule,
    FormsModule,
    HttpModule
];

export const ALL_PRE_CONFIGURATION = {
    declarations: DECLARATIONS,
    imports: IMPORTS,
    providers: [
        BaseRequestOptions,
        MockBackend,
        {
            provide: Http,
            useFactory: (backend, options) => new Http(backend, options),
            deps: [MockBackend, BaseRequestOptions]
        }
    ]
};

export default { ALL_PRE_CONFIGURATION, IMPORTS, DECLARATIONS };
